import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpService } from "./services/http.service";
// import { action } from "ui/dialogs";
require( "nativescript-localstorage" );

@Component({
  selector: "my-app",
  template: `<page-router-outlet></page-router-outlet>`,
  providers: [HttpService]
})
export class AppComponent {
  constructor(
    private routerExtensions: RouterExtensions,
    private http: HttpService
  ) {}

  public ngOnInit() {
    console.log( localStorage.getItem("token"));
    if( localStorage.getItem("token") != null ) {
      this.routerExtensions.navigate(["/home"], { clearHistory: true });
    }else{
      this.http.initiate(()=>{
        this.routerExtensions.navigate(["/home"], { clearHistory: true });
      });
    }
  }
}

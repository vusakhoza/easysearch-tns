import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { Observable as RxObservable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from "rxjs/Observable";
import { RouterExtensions } from "nativescript-angular/router";
import { LoadingIndicator } from "nativescript-loading-indicator";
import { Global } from "./global.service";

require( "nativescript-localstorage" );

@Injectable()
export class HttpService extends Global {

    // extends Global service so inherits propoerties and methods 
    // like http, host, roothost and routerExtensions

    private dialogs = require("ui/dialogs");
    public loader = new LoadingIndicator();
    
    public host = this.host;
    public roothost = this.roothost;

    private _http = require("http");

    public deletePost (id) {
        this.loader.show();
        let __this = this;
        let url = this.host + "/delete/";

        console.log(id);
        var form = new FormData();
        form.append("id", id);

        this._http.request({
            url: url,
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "JWT " + localStorage.getItem("token"),
            },
            content: form
        }).then(function (response) {
            console.log(__this.showObject(response));
            let _data = JSON.parse(response.content);

            if( response.statusCode == 200 ){
                if( _data.success ){
                    console.log("post deleted");
                    __this.routerExtensions.navigate(["/manage"]); 
                    __this.routerExtensions.navigate(["/resetmanage"]); 
                }else{
                    console.log(_data.errors);
                    let errors = "";
                    for(var e in _data.errors) errors += _data.errors[e] + "\n";
                    __this.showDialog(errors);
                }
            }else if( response.statusCode == 403 ){
                __this.initiate(()=>{
                    __this.deletePost (id);
                });
            }
            __this.loader.hide();
        }, function (e) {
            __this.loader.hide();
            console.log("Error occurred " + e);
            __this.showDialog("Err:Profile. There was a problem with your request. Please try again!");
        });
    }

    public updateProfile (scope) {
        this.loader.show();
        let __this = this;
        let url = this.host + "/profile/";

        var form = new FormData();
        form.append("name", scope.profile.name);
        form.append("phone", scope.profile.phone);

        this._http.request({
            url: url,
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "JWT " + localStorage.getItem("token")
            },
            content: form
        }).then(function (response) {
            console.log(__this.showObject(response));
            let _data = JSON.parse(response.content);

            if( response.statusCode == 200 ){
                if( _data.success ){
                    scope.profile.name = _data.userdets.fields.name;
                    scope.profile.phone = _data.userdets.fields.phone;

                    localStorage.setItem("name", _data.userdets.fields.name);
                    localStorage.setItem("phone", _data.userdets.fields.phone);
                    console.log("updated successfully");
                }else{
                    console.log(_data.errors);
                    let errors = "";
                    for(var e in _data.errors) errors += _data.errors[e] + "\n";
                    __this.showDialog(errors);
                }
            }else if( response.statusCode == 403 ){
                __this.initiate(()=>{
                    __this.updateProfile (scope);
                });
            }
            __this.loader.hide();
        }, function (e) {
            __this.loader.hide();
            console.log("Error occurred " + e);
            __this.showDialog("Err:Profile. There was a problem with your request. Please try again!");
        });
    }

    public getProfile (scope) {
        this.loader.show();
        let __this = this;
        let url = this.host + "/profile/";

        this._http.request({
            url: url,
            method: "GET",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "JWT " + localStorage.getItem("token")
            },
            // content: form
        }).then(function (response) {
            console.log(__this.showObject(response));
            let _data = JSON.parse(response.content);

            if( response.statusCode == 200 ){
                if( _data.success ){
                    scope.profile = _data.message.fields.userdets[0].fields;
                }else{
                    console.log(_data.errors);
                    let errors = "";
                    for(var e in _data.errors) errors += _data.errors[e] + "\n";
                    __this.showDialog(errors);
                }
            }else if( response.statusCode == 403 ){
                __this.initiate(()=>{
                    __this.getProfile (scope);
                });
            }
            __this.loader.hide();
        }, function (e) {
            __this.loader.hide();
            console.log("Error occurred " + e);
            __this.showDialog("Err:Profile. There was a problem with your request. Please try again!");
        });
    }

    public checkCode (code, phonenumber, username, password) {
        this.loader.show();
        let __this = this;
        let url = this.host + "/profile/?checkcode="+code+"&phone="+phonenumber;

        this._http.request({
            url: url,
            method: "GET",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "JWT " + localStorage.getItem("token")
            },
            // content: form
        }).then(function (response) {
            console.log(__this.showObject(response));
            let _data = JSON.parse(response.content);

            if( response.statusCode == 200 ){
                if( _data.success ){
                    localStorage.setItem("username", username);
                    localStorage.setItem("password", password);
                    __this.initiate(()=>{
                        __this.routerExtensions.navigate(['/home', {tab: 2}], { clearHistory: true });
                    });

                }else{
                    console.log(_data.errors);
                    let errors = "";
                    for(var e in _data.errors) errors += _data.errors[e] + "\n";
                    __this.showDialog(errors);
                }
            }else if( response.statusCode == 403 ){
                __this.initiate(()=>{
                    __this.checkCode (code, phonenumber, username, password);
                });
            }
            __this.loader.hide();
        }, function (e) {
            __this.loader.hide();
            console.log("Error occurred " + e);
            __this.showDialog("Err:Check code. There was a problem with your request. Please try again!");
        });
    }

    public registerPhone (phonenumber) {
        this.loader.show();
        let __this = this;
        let url = this.host + "/profile/?confirm="+phonenumber;

        this._http.request({
            url: url,
            method: "GET",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "JWT " + localStorage.getItem("token")
            },
            // content: form
        }).then(function (response) {
            console.log(__this.showObject(response));
            let _data = JSON.parse(response.content);

            if( response.statusCode == 200 ){
                if( _data.success ){
                    // localStorage.setItem("username", _data.userdets.username);
                    // localStorage.setItem("password", _data.userdets.key);
                    __this.routerExtensions.navigate(['/confirm', {"phonenumber": phonenumber, "username":_data.userdets.username,"password":_data.userdets.key}]);
                }else{
                    console.log(_data.errors);
                    let errors = "";
                    for(var e in _data.errors) errors += _data.errors[e] + "\n";
                    __this.showDialog(errors);
                }
            }else if( response.statusCode == 403 ){
                __this.initiate(()=>{
                    __this.registerPhone(phonenumber);
                });
            }
            __this.loader.hide();
        }, function (e) {
            __this.loader.hide();
            console.log("Error occurred " + e);
            __this.showDialog("Err:Create Post. There was a problem with your request. Please try again!");
        });
    }

    public creatPost (body, func?, edit?) {
        this.loader.show();
        let __this = this;
        let url = this.host + "/posts/";
        let type = ( edit ) ? "PUT" : "POST";

        var form = new FormData();
        form.append("title", body.title);
        form.append("description", body.description);
        form.append("category", body.category);
        form.append("location", body.location);
        form.append("name", body.name);
        form.append("phone", body.phone);
        form.append("price", body.price);
        if( edit ) form.append("id", body.id);

        this._http.request({
            url: url,
            method: type,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "JWT " + localStorage.getItem("token")
            },
            content: form
        }).then(function (response) {
            console.log(__this.showObject(response));
            let _data = JSON.parse(response.content);

            if( response.statusCode == 200 ){
                if( _data.success ){
                    if( typeof func == "function" ) func(_data.postid);
                }else{
                    console.log(_data.errors);
                    let errors = "";
                    for(var e in _data.errors) errors += _data.errors[e] + "\n";
                    __this.showDialog(errors);
                }
            }else if( response.statusCode == 403 ){
                __this.initiate(()=>{
                    __this.creatPost(body, (func) ? func : "");
                });
            }
            __this.loader.hide();
        }, function (e) {
            __this.loader.hide();
            console.log("Error occurred " + e);
            __this.showDialog("Err:Create Post. There was a problem with your request. Please try again!");
        });
    }

    public getCategories (scope, func?) {
        // if(bool) this.loader.show();
        let __this = this;
        let url = this.host + "/categories/";
        this._http.request({
            url: url,
            method: "GET",
            headers: {
                "Content-Type": "multipart/form-data",
                "Authorization": "JWT " + localStorage.getItem("token")
            },
            // content: body
        }).then(function (response) {
            console.log(__this.showObject(response));
            let _data = JSON.parse(response.content);
            console.log(_data.length);
            for( let i in _data ) {
                console.log(_data[i].fields.name);
                scope.categories.push(_data[i].fields.name);
            }
            console.dir(scope.categories);

            if( typeof func == "function" ) func();

            __this.loader.hide();
        }, function (e) {
            __this.loader.hide();
            console.log("Error occurred " + e);
            __this.showDialog("Err:Category. There was a problem with your request. Please try again!");
        });
    }

    public getPosts(scope, search, func, offset, loader, forself?) {
        if(loader) this.loader.show();
        let __this = this;
        let searchvar = ( search != "" ) ? "&search=" + search : "";// if a search paramter is provided
        let forselfFlag = ( forself ) ? "forself&" : "";// if we want this particular users ads only
        let url = this.host + "/posts/?" +forselfFlag+ "offset=" + offset + searchvar;

        console.log(url);
        console.log(localStorage.getItem("token"));

        this._http.request({
            url: url,
            method: "GET",
            headers: {
                "Content-Type": "multipart/form-data",
                "Authorization": "JWT " + localStorage.getItem("token")
            },
            // content: body
        }).then(function (response) {

            console.log(__this.showObject(response));
            if( response.statusCode == 200 ){
                let _data = JSON.parse(response.content);
                scope.posts = _data;
            }else if( response.statusCode == 403 ){
                __this.initiate(()=>{
                    __this.getPosts(scope, search, func, offset, loader, forself);
                });
            }
            __this.loader.hide();
        }, function (e) {
            __this.loader.hide();
            console.log("Error occurred " + e);
            __this.showDialog("Err:Posts. There was a problem with your request. Please try again!");
        });
    }

    public initiate(func?) {
        this.loader.show();
        let username = ( localStorage.getItem("username") != null ) ? localStorage.getItem("username") : "admin";
        let password = ( localStorage.getItem("password") != null ) ? localStorage.getItem("password") : "Lmbo_8989";

        var data = JSON.stringify({"username": username,"password": password,});
        console.log("Login data:");
        console.log(data);

        let __this = this;
        let url = this.host + "/api-token-auth/";

        this._http.request({
            url: url,
            method: "POST",
            headers: { 
                "Content-Type": "application/json; charset=utf-8",
            },
            content: data
        }).then(function (response) {

            console.log(__this.showObject(response));
            let _data = JSON.parse(response.content);
            localStorage.setItem("token", _data.token);
            console.log(_data.token);
            if ( typeof func === 'function' ) func();
            // if( response.statusCode == 201 ){
            //     __this.routerExtensions.navigate(["/home"]);
            // }else {
            //     __this.showDialog("Something went wrong. Please try again in a few minutes! If problem persists contact administration.");
            // }

            __this.loader.hide();
        }, function (e) {
            __this.loader.hide();
            localStorage.clear();
            console.log("Err:Initiate. Error occurred " + e);
            // __this.showDialog("There was a problem with your request. Please try again!");
        });
    }

    public showDialog(message:any) {
        this.dialogs.alert(message).then(()=> {
            console.log("Dialog closed!");
        });
    }

    private handleError (error: Response | any) {
        console.error('ApiService::handleError', error);
        console.log(error);
    }

}
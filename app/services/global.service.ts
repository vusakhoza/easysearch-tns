import { Injectable } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Http } from "@angular/http";
require("nativescript-localstorage");

@Injectable()
export class Global {
    public host = 'http://easysearch.cherub.co.zw/api';
    public roothost = 'http://easysearch.cherub.co.zw';
    // public host = 'http://93.188.166.194:3050/api';
    // public roothost = 'http://93.188.166.194:3050';

    constructor(
        public routerExtensions: RouterExtensions,
        public http: Http,
    ) {}

    public loggedIn(){
        if( localStorage.getItem("username") != null && localStorage.getItem("password") != null ) return true;
        else return false;
    }

    public showObject(obj) {
        var result = "";
        for (var p in obj) {
            if( obj.hasOwnProperty(p) ) {
                result += p + " , " + obj[p] + "\n";
            } 
        }           
        return result;
    }

    public back() {
        this.routerExtensions.back();
    }

    public goHome() {
        this.routerExtensions.navigate(["/home"], { clearHistory: true });
    }
}
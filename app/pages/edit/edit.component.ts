import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Global } from "../../services/global.service";
import { HttpService } from "../../services/http.service";
import { PageRoute } from "nativescript-angular/router";
import "rxjs/add/operator/switchMap";
import * as platformModule from "tns-core-modules/platform";
import * as TNSPhone from 'nativescript-phone';
import { HomeComponent } from "../home/home.component";
import { action } from "tns-core-modules/ui/dialogs";
import { ImageSource } from "tns-core-modules/image-source";

require( "nativescript-localstorage" );
const imageSource = require("image-source");

@Component({
    selector: "my-app",
    templateUrl: "./pages/edit/edit.component.html",
    providers: [Global, HttpService]
})
export class EditPostComponent extends HomeComponent {

    //extends HomeComponent so inherits all public methods and properties 
    //like newpost, createAd, takepic etc, from there
    //newpost is in fact the post to be edited

    public roothost = this.global.roothost;
    public oldimages = {
        image1: "",
        image2: "",
        image3: "",
    }

    public ngOnInit() {

        //get route params
        let __this = this;
        this.pageRoute.activatedRoute
        .switchMap(activatedRoute => activatedRoute.params)
        .forEach((params) => {
            console.log(__this.global.showObject(params));
            this.newpost.title = params["title"];
            this.newpost.description = params["description"];
            this.newpost.category = params["category"];
            this.newpost.location = params["location"];
            this.newpost.name = params["ownername"];
            this.newpost.phone = params["phone"];
            this.newpost.price = params["price"];
            this.newpost.image1 = params["image1"];
            this.newpost.image2 = params["image2"];
            this.newpost.image3 = params["image3"];
            this.newpost.id = params["id"];
        });

        console.log("images loading");
        if( this.newpost.image1 != "" ) {
            imageSource.fromUrl(this.roothost + this.newpost.image1)
            .then((res)=>{
                console.dir(res);
                console.log("Image successfully loaded");
                this.images.imageSource1 = res;
            }, (error)=>{
                console.log("Error loading image: " + error);
            });
        }
        else this.images.imageSource1.fromResource("newimage");

        if( this.newpost.image2 != "" ) {
            imageSource.fromUrl(this.roothost + this.newpost.image2)
            .then((res)=>{
                console.dir(res);
                console.log("Image successfully loaded");
                this.images.imageSource2 = res;
            }, (error)=>{
                console.log("Error loading image: " + error);
            });
        }
        else this.images.imageSource2.fromResource("newimage");

        if( this.newpost.image3 != "" ) {
            imageSource.fromUrl(this.roothost + this.newpost.image3)
            .then((res)=>{
                console.dir(res);
                console.log("Image successfully loaded");
                this.images.imageSource3 = res;
            }, (error)=>{
                console.log("Error loading image: " + error);
            });
        }
        else this.images.imageSource3.fromResource("newimage");
    
    }

    public updateAd() {
        console.dir(this.newpost);
        //user has to select category first
        if( this.newpost.category != 0 ){
            this.http.creatPost(this.newpost, (id)=>{
                console.log("PostID: " + id);

                Promise.resolve(123)
                .then((res)=>{
                    console.log("first image");
                    if( this.images.image1Changed ) 
                        this.saveImageAndSend(this.images.imageSource1, "image1", id.toString(), true);
                })
                .then((res)=>{
                    console.log("second image");
                    if( this.images.image2Changed ) 
                        this.saveImageAndSend(this.images.imageSource2, "image2", id.toString(), true);
                })
                .then((res)=>{
                    console.log("third image");
                    if( this.images.image3Changed ) 
                        this.saveImageAndSend(this.images.imageSource3, "image3", id.toString(), true);
                })
                .catch((err) => {
                    console.log(err.message); // something bad happened
                    this.http.showDialog("There was a problem with one of the images!" + err.message);
                });

                //if no images uploaded
                if( !this.images.image1Changed && !this.images.image2Changed && !this.images.image3Changed ) {
                    console.log("images done");
                    this.zone.run(()=>{
                        this.http.loader.hide();
                        this.tabSelectedIndex = 0;
                        this.resetForm();
                        this.routerExtensions.navigate(["/manage"]);
                    });
                }
            }, true);// take note of the true tag that indicates that this is an edit not a new post
        }else{
            this.http.showDialog("Please choose a category!");
        }
    }

}
import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Global } from "../../services/global.service";
import { HttpService } from "../../services/http.service";
import { PageRoute } from "nativescript-angular/router";
import "rxjs/add/operator/switchMap";
import * as platformModule from "tns-core-modules/platform";
import * as TNSPhone from 'nativescript-phone';
import { HomeComponent } from "../home/home.component";
import { action, confirm } from "tns-core-modules/ui/dialogs";

require( "nativescript-localstorage" );

@Component({
    selector: "my-app",
    templateUrl: "./pages/manage/manage.component.html",
    providers: [Global, HttpService]
})
export class ManagePostsComponent extends HomeComponent {

    //extends HomeComponent so inherits all public methods and properties 
    //like newpost, posts etc, from there
    public ngOnInit() {

        this.http.getPosts(this, "", null, 0, true, true);// get posts for this user

    }

    public manage(fields, id) {
        let __this = this;
        let options = {
            title: "Manage Options",
            message: "Choose an option",
            cancelButtonText: "Cancel",
            actions: ["Edit", "Delete"]
        };

        action(options).then((result) => {
            console.log(result);
            console.dir(fields);
            fields.id = id;// add post id
            switch(result){
                case "Edit":
                    __this.routerExtensions.navigate(["/edit", fields]);
                break;
                case "Delete":
                    __this.deletePost(id);
                break;
            }
        });
    }

    public deletePost(id) {
        console.log(id);
        let options = {
            title: "Warning",
            message: "You are about to delete a post. This action cannot be undone!",
            okButtonText: "Sure, go ahead",
            cancelButtonText: "No",
            neutralButtonText: "Cancel"
        };

        confirm(options).then((result: boolean) => {
            console.log(result);
            if( result ) this.http.deletePost(id);
        });
    }

    public refreshPostData(args) {
        var pullRefresh = args.object;
        this.http.getPosts(this, "", ()=>{}, 0, false, true);
        setTimeout(function () {
            pullRefresh.refreshing = false;
        }, 1000);
    }

    public loadMorePosts() {
        var offset = this.posts.length;
        log(offset);
        this.http.getPosts(this, "", null, offset, true, true);
    }
    
}

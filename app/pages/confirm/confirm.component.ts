import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Global } from "../../services/global.service";
import { HttpService } from "../../services/http.service";
import { PageRoute } from "nativescript-angular/router";
import "rxjs/add/operator/switchMap";
import * as platformModule from "tns-core-modules/platform";

require( "nativescript-localstorage" );

@Component({
    moduleId: module.id,
    selector: "my-app",
    templateUrl: "confirm.component.html",
    providers: [Global, HttpService]
})
export class ConfirmComponent {

    constructor(
        private routerExtensions: RouterExtensions,
        private pageRoute: PageRoute,
        private global: Global,
        private http: HttpService
    ) {}

    public confirmationCode: any;
    public phonenumber: any;
    public username: string;
    public password: string;
    public screenWidth = platformModule.screen.mainScreen.widthPixels;

    public ngOnInit() {

        let __this = this;
        this.pageRoute.activatedRoute
        .switchMap(activatedRoute => activatedRoute.params)
        .forEach((params) => {
            console.log(__this.global.showObject(params));
            __this.phonenumber = params["phonenumber"];
            __this.username = params["username"];
            __this.password = params["password"];
        });

    }

    public checkCode() {
        if( this.confirmationCode != "" )
            this.http.checkCode(this.confirmationCode, this.phonenumber, this.username, this.password);
        else
            this.http.showDialog("Please enter a valid confirmation code!");
    }
}

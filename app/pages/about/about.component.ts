import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Global } from "../../services/global.service";
import { HttpService } from "../../services/http.service";

const utilityModule = require("utils/utils");

require( "nativescript-localstorage" );

@Component({
    selector: "my-app",
    template: `
        <ActionBar title="About Easysearch">
            <NavigationButton text="Go Back" android.systemIcon="ic_menu_back" (tap)="global.back()"></NavigationButton>
        </ActionBar>
        <StackLayout>
            <Label class="m-30" textWrap="true" text="Easysearch is the brain child of a small team of developers who call themselves Cherub, named after an arch angel and symbolising the earnestness and purity of their ideas. The idea behind Easysearch is to make advertising your goods as simple and effortless as possible whilst trying to leverage the power of modern technology to give relevant statistics on whether or not the chosen advertising channels are working for the average user. The concept is still in its infancy and more can be expected soon."></Label>
            <Label class="m-30" text="Cherub Website" (tap)="gotoCherub()" style="color:blue;"></Label>
        <StackLayout>
    `,
    providers: [Global, HttpService]
})
export class AboutComponent {

    constructor(
        public routerExtensions: RouterExtensions,
        public global: Global,
        public http: HttpService,
    ) {}
    
    public gotoCherub(){
        utilityModule.openUrl("https://www.cherub.co.zw");
    }
}

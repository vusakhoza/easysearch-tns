import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Global } from "../../services/global.service";
import { HttpService } from "../../services/http.service";
import { alert, confirm } from "tns-core-modules/ui/dialogs";

require( "nativescript-localstorage" );

@Component({
    selector: "my-app",
    templateUrl: "./pages/profile/profile.component.html",
    providers: [Global, HttpService]
})
export class ProfileComponent {

    constructor(
        private routerExtensions: RouterExtensions,
        private global: Global,
        private http: HttpService
    ) {}

    public profile = {
        name: "",
        phone: ""
    };

    public ngOnInit() {
        this.http.getProfile(this);
    }

    public submit(scope) {
        this.http.updateProfile(this);
    }

    public where() {
        let options = {
            title: "Account Info",
            message: "Easysearch automatically creates an account for you when you either submit a post with a new unique number or you confirm your number for the first time.",
            okButtonText: "Got it",
        };

        alert(options).then(() => {
            console.log("Got it");
        });
    }

    public diffNumber() {
        let options = {
            title: "Warning",
            message: `Confirming a different number will sign you out of this account and remove access to editing ads created using this account. You can regain access by confirming your number again.`,
            okButtonText: "Yes",
            cancelButtonText: "No",
            // neutralButtonText: "Cancel"
        };

        confirm(options).then((result: boolean) => {
            console.log(result);
            //if yes then logout
            if( result ) {
                localStorage.clear();
                this.routerExtensions.navigate(["/home", {tab: 2}], { clearHistory: true });
            }
        });
    }

}

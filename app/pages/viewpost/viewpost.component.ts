import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Global } from "../../services/global.service";
import { HttpService } from "../../services/http.service";
import { PageRoute } from "nativescript-angular/router";
import "rxjs/add/operator/switchMap";
import * as platformModule from "tns-core-modules/platform";

require( "nativescript-localstorage" );

@Component({
    selector: "my-app",
    templateUrl: "./pages/viewpost/viewpost.component.html",
    providers: [Global, HttpService]
})
export class ViewPostComponent {

    constructor(
        private routerExtensions: RouterExtensions,
        private pageRoute: PageRoute,
        private global: Global,
        private http: HttpService
    ) {}

    public post: any;
    public screenWidth = platformModule.screen.mainScreen.widthPixels;
    public phone = require("nativescript-phone");

    public ngOnInit() {

        let __this = this;
        this.pageRoute.activatedRoute
        .switchMap(activatedRoute => activatedRoute.params)
        .forEach((params) => {
            console.log(__this.global.showObject(params));
            __this.post = params;
        });

    }

    /// Dial a phone number.
    public callNumber(number) {
        this.phone.requestCallPermission();
        console.log(number);
        this.phone.dial(number, false);
    }
    
    // Text a number (or multiple numbers)
    public sendSMS(number, message) {
        this.phone.sms([number], message)
            .then((args) => {
                console.log(JSON.stringify(args));
            }, (err) => {
                console.log('Error: ' + err);
            })
    }

}

import { Component, OnInit, NgZone  } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Global } from "../../services/global.service";
import { HttpService } from "../../services/http.service";
import { PageRoute } from "nativescript-angular/router";
import "rxjs/add/operator/switchMap";
import { registerElement } from "nativescript-angular/element-registry";
import * as platformModule from "tns-core-modules/platform";
import { ListPicker } from "tns-core-modules/ui/list-picker";
import * as camera from "nativescript-camera";
import { Image } from "tns-core-modules/ui/image";
import { ImageSource } from "tns-core-modules/image-source";
import * as enumsModule from "tns-core-modules/ui/enums"
import * as fsModule from "tns-core-modules/file-system";
import bgHttpModule = require('nativescript-background-http')
import { ImageAsset } from "tns-core-modules/image-asset";
import { isAndroid } from "tns-core-modules/platform";
import { SearchBar } from "tns-core-modules/ui/search-bar";

registerElement("PullToRefresh", () => require("nativescript-pulltorefresh").PullToRefresh);

require( "nativescript-localstorage" );

//image options and settings
const format = enumsModule.ImageFormat.jpeg

@Component({
    selector: "my-app",
    templateUrl: "./pages/home/home.component.html",
    providers: [Global, HttpService]
})
export class HomeComponent {

    public categories = [
        "-- Select a Category --",
        "Electronics",
        "Fashion",
        "Business",
        "Property",
        "Services",
        "Jobs",
        "Vehicles",
        "Sports & Hobbies",
        "Academic"
    ];

    constructor(
        public routerExtensions: RouterExtensions,
        public global: Global,
        public http: HttpService,
        public zone: NgZone,
        public pageRoute: PageRoute,
    ) {}

    public tabSelectedIndex = 0;
    public screenWidth = platformModule.screen.mainScreen.widthPixels;
    public quarterScreenWidth = this.screenWidth / 4.5;
    public posts: any[];
    public newpost: any = {
        title:"",
        description:"",
        category: 1,
        location:"",
        name:"",
        phone:"",
        price:"",
    };
    public images = {
        imageSource1: new ImageSource(),
        imageSource2: new ImageSource(),
        imageSource3: new ImageSource(),
        image1Changed: false,
        image2Changed: false,
        image3Changed: false,
    }
    public confirmNumber: string = "";
    public loggedIn: boolean = false;
    public searchPhrase: string = "";
    public showSearch = false;

    public ngOnInit() {
        //Home Tab Operations
        this.http.getPosts(this, "", ()=>{
            this.posts.push({"title":"Load more... (footer)"});
        }, 0, true);
        // setTimeout(()=>{
        //     this.http.getCategories(this);  
        // }, 1000);

        //New Post Tab Operations
        this.images.imageSource1.fromResource("newimage");
        this.images.imageSource2.fromResource("newimage");
        this.images.imageSource3.fromResource("newimage");
        if( this.global.loggedIn() ){
            if( localStorage.getItem("name") != null ) this.newpost.name = localStorage.getItem("name"); 
            if( localStorage.getItem("phone") != null ) this.newpost.phone = localStorage.getItem("phone"); 
        }

        //Profile Tab Operations
        this.loggedIn = this.global.loggedIn();

        //get route params
        let __this = this;
        this.pageRoute.activatedRoute
        .switchMap(activatedRoute => activatedRoute.params)
        .forEach((params) => {
            console.log(__this.global.showObject(params));
            this.zone.run(()=>{
                this.tabSelectedIndex = params["tab"]
            });
        });
    }

    public sBLoaded(args){
        let searchBar = <SearchBar>args.object;
        searchBar.dismissSoftInput();
        console.log(isAndroid);
        if (isAndroid) {
            searchBar.android.clearFocus();
            searchBar.android.setFocusable(false);
        }
    }

    public onClear(args){
        let searchBar = args.object;
        searchBar.text = "";
        this.http.getPosts(this, this.searchPhrase, ()=>{}, 0, true);
    }

    public onSubmit(args) {
        let searchBar = args.object;
        let searchValue = searchBar.text.toLowerCase();
        this.http.getPosts(this, searchValue, ()=>{}, 0, true);
    }

    public registerNumber() {
        if( this.confirmNumber != "" )
            this.http.registerPhone(this.confirmNumber);
        else
            this.http.showDialog("Please enter a valid mobile number!");
    }

    public saveImageAndSend(imageSource, image, postid, isedit?){
        //image save options
        this.http.loader.show();
        // let contentType = `image/${format}`;
        let savePath = fsModule.knownFolders.documents().path;
        let fileName = 'img_' + new Date().getTime() + '.' + format;
        let filePath = fsModule.path.join( savePath, fileName );

        if ( imageSource.saveToFile( filePath, format ) ) {
            var session = bgHttpModule.session('image-upload')

            var options = {
                url: this.global.host + "/posts/",
                method: 'POST',
                headers: {
                    'Content-Type': 'application/octet-stream',
                    'Authorization': 'JWT ' + localStorage.getItem("token"),
                },
                description: "{'uploading': 'File' }"
            }

            var params = [
                { name: "image", value: image },
                { name: "addimages", value: 'true' },
                { name: "postid", value: postid },
                { name: "file", filename: filePath, mimeType: 'image/jpeg' }
            ];
            
            let task = session.multipartUpload(params, options);

            let logEvent = (e)=>{
                console.log(e.eventName);
                console.log(e.data);
                // on successfully uploading each image, reset stuff
                if (e.eventName == "responded") {
                    console.log("images done");
                    this.zone.run(()=>{
                        this.http.loader.hide();
                        this.tabSelectedIndex = 0;
                        this.resetForm();
                        if( !isedit ) this.http.getPosts(this, this.searchPhrase, ()=>{}, 0, true);
                        else this.routerExtensions.navigate(["/manage"]);
                    });
                }
            }

            task.on('progress', logEvent)
            task.on('error', logEvent)
            task.on('complete', logEvent)
            task.on("responded", logEvent);

        }
    }

    public takePic(imageNumber){
        camera.requestPermissions();
        let options = { width: 300, height: 300, keepAspectRatio: true }
        let __this = this;
        camera.takePicture(options)
        .then((imageAsset) => {
            console.log("Result is an image asset instance");
            console.log(__this.global.showObject(imageAsset));
            let img = new ImageSource();
            //run the function with zone in order to refresh scope
            //convert asset to resource so that image element recognizes it.
            __this.zone.run(()=>{
                switch(imageNumber){
                    case "image1":
                        img.fromAsset(imageAsset).then((source)=>{
                            __this.images.imageSource1 = source;
                            __this.images.image1Changed = true;
                        });
                        break;
                    case "image2":
                        img.fromAsset(imageAsset).then((source)=>{
                            __this.images.imageSource2 = source;
                            __this.images.image2Changed = true;
                        });
                        break;
                    case "image3":
                        img.fromAsset(imageAsset).then((source)=>{
                            __this.images.imageSource3 = source;
                            __this.images.image3Changed = true;
                        });
                        break;
                }
            });
        }).catch((err) => {
            console.log("Error -> " + err.message);
        });
    }

    public createAd() {
        // if( this.global.loggedIn() ){
            console.dir(this.newpost);
            //user has to select category first
            if( this.newpost.category != 0 ){
                this.http.creatPost(this.newpost, (id)=>{
                    console.log("PostID: " + id);

                    Promise.resolve(123)
                    .then((res)=>{
                        console.log("first image");
                        if( this.images.image1Changed ) 
                            this.saveImageAndSend(this.images.imageSource1, "image1", id.toString());
                    })
                    .then((res)=>{
                        console.log("second image");
                        if( this.images.image2Changed ) 
                            this.saveImageAndSend(this.images.imageSource2, "image2", id.toString());
                    })
                    .then((res)=>{
                        console.log("third image");
                        if( this.images.image3Changed ) 
                            this.saveImageAndSend(this.images.imageSource3, "image3", id.toString());
                    })
                    .catch((err) => {
                        console.log(err.message); // something bad happened
                        this.http.showDialog("There was a problem with one of the images!" + err.message);
                    });

                    //if no images uploaded
                    if( !this.images.image1Changed && !this.images.image2Changed && !this.images.image3Changed ) {
                        console.log("images done");
                        this.zone.run(()=>{
                            this.http.loader.hide();
                            this.tabSelectedIndex = 0;
                            this.resetForm();
                            this.http.getPosts(this, this.searchPhrase, ()=>{}, 0, true);
                        });
                    }
                });
            }else{
                this.http.showDialog("Please choose a category!");
            }
        // }else{
        //     this.zone.run(()=>{
        //         this.tabSelectedIndex = 2;
        //     });
        // }
    }

    public resetForm(){
        this.newpost = { title:"", description:"", category: 1, location:"", name:"", phone:"", price:"" };
        this.images = { imageSource1: new ImageSource(), imageSource2: new ImageSource(), imageSource3: new ImageSource(),
            image1Changed: false, image2Changed: false, image3Changed: false }
        this.images.imageSource1.fromResource("newimage");
        this.images.imageSource2.fromResource("newimage");
        this.images.imageSource3.fromResource("newimage");
    }

    public loadMoreItems() {
        var offset = this.posts.length;
        console.log(offset);
        if( this.posts.length > 20 ) this.http.getPosts(this, this.searchPhrase, null, offset, true);
    }

    public selectedIndexChanged(args) {
        let picker = <ListPicker>args.object;
        // console.log("picker selection: " + picker.selectedIndex);
        // console.log(this.categories[picker.selectedIndex]);
    }

    public refreshCategories(args) {
        var pullRefresh = args.object;
        this.categories = [];
        this.http.getCategories(this);
        setTimeout(function () {
            pullRefresh.refreshing = false;
        }, 1000);
    }

    public refreshPostData(args) {
        var pullRefresh = args.object;
        this.http.getPosts(this, this.searchPhrase, ()=>{}, 0, false);
        setTimeout(function () {
            pullRefresh.refreshing = false;
        }, 1000);
    }

    public signout(){
        localStorage.clear();
        this.routerExtensions.navigate(["/reset"]);
    }
}

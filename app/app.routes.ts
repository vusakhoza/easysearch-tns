import { AppComponent } from "./app.component";
import { HomeComponent } from "./pages/home/home.component";
import { ViewPostComponent } from "./pages/viewpost/viewpost.component";
import { ConfirmComponent } from "./pages/confirm/confirm.component";
import { ProfileComponent } from "./pages/profile/profile.component";
import { ManagePostsComponent } from "./pages/manage/manage.component";
import { EditPostComponent } from "./pages/edit/edit.component";
import { AboutComponent } from "./pages/about/about.component";

export const routes = [
    { path: "about", component: AboutComponent },
    { path: "edit", component: EditPostComponent },
    { path: "resetmanage", component: ManagePostsComponent },
    { path: "manage", component: ManagePostsComponent },
    { path: "profile", component: ProfileComponent },
    { path: "confirm", component: ConfirmComponent },
    { path: "viewpost", component: ViewPostComponent },
    { path: "home", component: HomeComponent },
    { path: "reset", component: HomeComponent },
    // { path: "", component: HomeComponent },
]; 
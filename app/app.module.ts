import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { NativeScriptFormsModule } from "nativescript-angular/forms"
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { routes } from "./app.routes";
import { MomentModule } from 'angular2-moment';
import { TNSFrescoModule } from "nativescript-fresco/angular";
import * as frescoModule from "nativescript-fresco";
import * as applicationModule from "tns-core-modules/application";

if (applicationModule.android) {
    applicationModule.on("launch", () => {
        frescoModule.initialize();
    });
}

import { AppComponent } from "./app.component";
import { HomeComponent } from "./pages/home/home.component";
import { ViewPostComponent } from "./pages/viewpost/viewpost.component";
import { ConfirmComponent } from "./pages/confirm/confirm.component";
import { ProfileComponent } from "./pages/profile/profile.component";
import { ManagePostsComponent } from "./pages/manage/manage.component";
import { EditPostComponent } from "./pages/edit/edit.component";
import { AboutComponent } from "./pages/about/about.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ViewPostComponent,
    ConfirmComponent,
    ProfileComponent,
    ManagePostsComponent,
    EditPostComponent,
    AboutComponent,
  ],
  bootstrap: [AppComponent],
  imports: [
    NativeScriptHttpModule ,
    NativeScriptModule,
    NativeScriptRouterModule,
    NativeScriptRouterModule.forRoot(routes),
    NativeScriptFormsModule,
    MomentModule,
    TNSFrescoModule,  
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {}
